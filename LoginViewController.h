//
//  LoginViewController.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textUserName;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogin;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelUsername;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UIButton *buttonSignUp;
@property (weak, nonatomic) IBOutlet UISwitch *switchUserSave;

- (IBAction)pressedLogin:(id)sender;
- (IBAction)pressedKeyKill:(id)sender;
- (IBAction)pressedSignUp:(id)sender;


@end

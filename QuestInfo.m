//
//  QuestInfo.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "QuestInfo.h"

@implementation QuestInfo


+(NSMutableArray *)questInfo
{
    //created an array
    NSMutableArray *questDetails = [[NSMutableArray alloc]init];
    
    //created a custom object to load info into the array
    QuestInfo *questObject = [[QuestInfo alloc]init];
    
    //populated array with custom object properties
    questObject.questName = @"Bandits in the Woods";
    questObject.questAlign = @"Good";
    questObject.questDescript = @"The Famed Bounty hunter HotDog has requested the aid of a hero in ridding the woods of terrifying bandits who have thus far eluded his capture, as he is actually a dog, and cannot actually grab things more than 6 feet off the ground.";
    questObject.questExp = @"150 EXP";
    questObject.questGiverImage = [UIImage imageNamed:@"DoggBounty.jpg"];
    questObject.questGiverName = @"HotDogg The Bounty Hunter";
    questObject.questGiverLat = 46.8541979;
    questObject.questGiverLong = -96.8285138;
    questObject.questLocLat = 46.908588;
    questObject.questLocLong = -96.808991;
    questObject.arrayIndex = 0;
    [questDetails addObject:questObject];
    
    questObject = [[QuestInfo alloc]init];
    questObject.questName = @"Special Delivery";
    questObject.questAlign = @"Nuetral";
    questObject.questDescript = @"Sir Jimmy was once the fastest man in the kingdom, brave as any soldier and wise as a king. Unfortunately, age catches us all in the end, and he has requested that I, his personal scribe, find a hero to deliver a package of particular importance--and protect it with their life.";
    questObject.questExp = @"100 EXP";
    questObject.questGiverImage = [UIImage imageNamed:@"jimmy.jpg"];
    questObject.questGiverName = @"Sir Jimmy The Swift";
    questObject.questGiverLat = 46.8739748;
    questObject.questGiverLong = -96.806112;
    questObject.questLocLat = 46.8657639;
    questObject.questLocLong = -96.7363173;
    questObject.arrayIndex = 1;
    [questDetails addObject:questObject];
    
    questObject = [[QuestInfo alloc]init];
    questObject.questName = @"Filthy Mongrel";
    questObject.questAlign = @"Evil";
    questObject.questDescript = @"That strange dog that everyone is treating like a bounty-hunter must go. By the order of Prince Jack, that smelly, disease ridden mongrel must be removed from our streets by any means necessary. He is disrupting the lives of ordinary citizens, and it's just really weird. Make it gone.";
    questObject.questExp = @"250 EXP";
    questObject.questGiverImage = [UIImage imageNamed:@"princeJack.jpg"];
    questObject.questGiverName = @"Prince Jack, The Iron Horse";
    questObject.questGiverLat = 46.8739748;
    questObject.questGiverLong = -96.806112;
    questObject.questLocLat = 46.892386;
    questObject.questLocLong = -96.799669;
    questObject.arrayIndex = 2;
    [questDetails addObject:questObject];
    
    return questDetails;
}

@end

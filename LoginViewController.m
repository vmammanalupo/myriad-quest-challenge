//
//  LoginViewController.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Set the login image
    _imageLogin.image = [UIImage imageNamed:@"login.jpg"];
    
    //Set title Label
    _labelTitle.font = [UIFont fontWithName:@"Chalkduster" size:30];
    _labelTitle.textColor = [UIColor purpleColor];
    _labelTitle.text = @"Myriad Quest";
    
    //set username label
    _labelUsername.font = [UIFont fontWithName:@"Chalkduster" size:14];
    _labelUsername.textColor = [UIColor purpleColor];
    _labelUsername.backgroundColor = [UIColor blackColor];
    
    //set button label
    _buttonLogin.titleLabel.textColor = [UIColor purpleColor];
    _buttonSignUp.titleLabel.textColor = [UIColor purpleColor];
    _buttonSignUp.titleLabel.backgroundColor = [UIColor blackColor];


    //setting switch position
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    if ([username length])
    {
        [_switchUserSave setOn:YES];
    }
    else
    {
        [_switchUserSave setOn:NO];
    }
    
}

//method call to remove keyboard by Return key
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _textUserName.text = [defaults objectForKey:@"username"];
    _textPassword.text = @"";
    
    PFUser *currentUser = [PFUser currentUser];
     {
         if (currentUser)
         {
             [self performSegueWithIdentifier:@"segueToTable" sender:nil];
         }
     }
}

//Created Login button that acts as a segue if username/password is correct. If either are incorrect an error message will occur to inform the user.Added switch to save successfull username at login if switch is activated
- (IBAction)pressedLogin:(id)sender
{
    [PFUser logInWithUsernameInBackground:_textUserName.text password:_textPassword.text block:^(PFUser *user, NSError *error)
    {
        if (user)
        {
            if (_switchUserSave.on)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:_textUserName.text forKey:@"username"];
                [defaults setObject:[user objectForKey:@"name"] forKey:@"heroName"];
                [defaults synchronize];
            }
            else
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"" forKey:@"username"];
                [defaults setObject:[user objectForKey:@"name"] forKey:@"heroName"];
                [defaults synchronize];
            }
            
            if ([[user objectForKey:@"alignment"]intValue]==0)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"Good" forKey:@"Alignment"];
                [defaults synchronize];
            }
            else if ([[user objectForKey:@"alignment"]intValue]==1)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"Neutral" forKey:@"Alignment"];
                [defaults synchronize];
            }
            else if ([[user objectForKey:@"alignment"]intValue]==2)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"Evil" forKey:@"Alignment"];
                [defaults synchronize];
            }
            else
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"Neutral" forKey:@"Alignment"];
                [defaults synchronize];
            }
            
            [self performSegueWithIdentifier:@"segueToTable" sender:nil];
 
        }
        else
        {
        UIAlertView *loginFailed = [[UIAlertView alloc]initWithTitle:@"Login Failed"
        message:@"The username or password entered was incorrect"
        delegate:nil
        cancelButtonTitle:@"Okay"
        otherButtonTitles: nil];
            
        [loginFailed show];
        }
    }];
}

//invisible button to remove an active keyboard when an area on the screen is touched
- (IBAction)pressedKeyKill:(id)sender
{
    [_textUserName resignFirstResponder];
    [_textPassword resignFirstResponder];
}

//Send user to sign up page
- (IBAction)pressedSignUp:(id)sender
{
    [self performSegueWithIdentifier:@"segueToSignUp" sender:nil];
}

@end

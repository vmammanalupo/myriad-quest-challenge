//
//  QuestInfo.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestInfo : NSObject

//Custom Class Method to return NSMutable Array
+(NSMutableArray*)questInfo;

//Quest Information Properties
@property (nonatomic, strong) NSString *questName;
@property (nonatomic, strong) NSString *questAlign;
@property (nonatomic, strong) NSString *questDescript;
@property (nonatomic, strong) NSString *questGiverName;
@property (nonatomic, strong) NSString *questExp;
@property (nonatomic, strong) NSString *questAccepted;

@property (nonatomic, strong) UIImage *questGiverImage;

@property (nonatomic) double questGiverLat;
@property (nonatomic) double questGiverLong;
@property (nonatomic) double questLocLat;
@property (nonatomic) double questLocLong;

//used for quest status accept button in DetailVC
@property (nonatomic) int arrayIndex;


@end

//
//  VMLAnnotationsView.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/20/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "VMLAnnotationsView.h"


@implementation VMLAnnotationsView

-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    //annotation image
    VMLAnnotation *annotationVML = (VMLAnnotation*)annotation;
    
    if  ([annotationVML.name isEqualToString:@"Giver"])
    {
        self.image = [UIImage imageNamed:@"giver.png"];
    }
    else if ([annotationVML.name isEqualToString:@"Location"])
    {
        self.image = [UIImage imageNamed:@"blueX.jpg"];
    }

    //annotation button
    self.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    //view enabled
    self.enabled = YES;
    self.canShowCallout = YES;
    
    return self;
}

@end

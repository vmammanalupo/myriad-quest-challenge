//
//  VMLAnnotationsView.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/20/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "VMLAnnotation.h"

@interface VMLAnnotationsView : MKPinAnnotationView

-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier;


@end

//
//  VMLAnnotation.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/20/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface VMLAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *name;

-initWithPosition:(CLLocationCoordinate2D)coords;

@end

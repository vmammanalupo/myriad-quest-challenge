//
//  TableViewController.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "TableViewController.h"
#import "DetailsViewController.h"
#import "SettingsViewController.h"
#import <Parse/Parse.h>

@interface TableViewController ()

@end

@implementation TableViewController
{
    NSMutableArray *questDetails;
    NSMutableArray *filteredQuests;
    NSMutableArray *onlineQuestarray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    onlineQuestarray = [[NSMutableArray alloc]init];
    
    //populating the quests from parse
    PFQuery *query = [PFQuery queryWithClassName:@"Quests"];
    [query includeKey:@"questGiver"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
    {
        //created variable for quest array index referenced by accept button on DetailsVC
        int x = 3;
        for (PFObject *questInfo in objects)
        {
            QuestInfo *questsOnline = [[QuestInfo alloc]init];
            
            questsOnline.arrayIndex = x;
            x++;
            int alignment = [[questInfo objectForKey:@"alignment"]intValue];
            if (alignment == 0)
            {
                questsOnline.questAlign = @"Good";
            }
            else if (alignment == 1)
            {
                questsOnline.questAlign = @"Neutral";
            }
            else if (alignment == 2)
            {
                questsOnline.questAlign = @"Evil";
            }
            
            questsOnline.questName = [questInfo objectForKey:@"name"];
            questsOnline.questDescript = [questInfo objectForKey:@"description"];

            PFUser *giverName = [questInfo objectForKey:@"questGiver"];
            questsOnline.questGiverName = [giverName objectForKey:@"name"];
            
            PFGeoPoint *giverLocation1 = [giverName objectForKey:@"location"];
            questsOnline.questGiverLat = giverLocation1.latitude;
            questsOnline.questGiverLong = giverLocation1.longitude;
            
            PFGeoPoint *questLocation2 = [questInfo objectForKey:@"location"];
            questsOnline.questLocLat = questLocation2.latitude;
            questsOnline.questLocLong = questLocation2.longitude;
            
            //set static image and quest EXP for downloaded quests
            questsOnline.questExp = @"175 EXP";
            questsOnline.questGiverImage = [UIImage imageNamed:@"purpleKnight.jpg"];
            
            [onlineQuestarray addObject:questsOnline];
        }
         questDetails = [QuestInfo questInfo];
        [questDetails addObjectsFromArray:onlineQuestarray];
        [self searchingQuests];
    }];
}

//Fast enumeration instance method for filtering quests by alignment
-(void)searchingQuests
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    filteredQuests = [[NSMutableArray alloc]init];
    
    if ([[defaults objectForKey:@"Alignment"] isEqualToString:@"Neutral"])
    {
        filteredQuests = questDetails;
    }
    else if ([[defaults objectForKey:@"Alignment"]isEqualToString:@"Good"])
    {
        for (QuestInfo *x in questDetails)
        {
            if ([x.questAlign isEqualToString:@"Good"])
            {
                [filteredQuests addObject:x];
            }
        }
    }
    else if ([[defaults objectForKey:@"Alignment"]isEqualToString:@"Evil"])
    {
        for (QuestInfo *x in questDetails)
        {
            if ([x.questAlign isEqualToString:@"Evil"])
            {
                [filteredQuests addObject:x];
            }
        }
    }
    else
    {
        filteredQuests = questDetails;
    }
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    //customizing navigation controller attributes
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Chalkduster" size:16], NSFontAttributeName, nil];
    
    [self searchingQuests];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //Set the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Set the number of rows in the section.
    return [filteredQuests count];
}

//Configured the CustomTableViewCells
- (CustomTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    //make cells be recycled when scrolling occurs
    
    if (!cell)
    {
        cell = [[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    //set custom cell display
    cell.labelCell.text = ((QuestInfo*)[filteredQuests objectAtIndex:indexPath.row]).questName;
    cell.labelCell.font = [UIFont fontWithName:@"Chalkduster" size:16];
    cell.labelCell.textColor = [UIColor purpleColor];
    
    cell.labelSubtitle.text =((QuestInfo*)[filteredQuests objectAtIndex:indexPath.row]).questGiverName;
    cell.labelSubtitle.font = [UIFont fontWithName:@"Chalkduster" size:10];
    cell.labelSubtitle.textColor = [UIColor purpleColor];
    
    cell.labelEXP.text = ((QuestInfo*)[filteredQuests objectAtIndex:indexPath.row]).questExp;
    cell.labelEXP.font = [UIFont fontWithName:@"Chalkduster" size:10];
    cell.labelEXP.textColor = [UIColor purpleColor];
    
    cell.imageCell.image = ((QuestInfo*)[filteredQuests objectAtIndex:indexPath.row]).questGiverImage;
    
    return cell;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueToDetails"])
    {
        DetailsViewController *dvc = [segue destinationViewController];
        dvc.detailViewInfo = [filteredQuests objectAtIndex:self.tableView.indexPathForSelectedRow.row];
    }
    else if ([segue.identifier isEqualToString:@"segueToSettings"])
    {
        SettingsViewController *svc = [segue destinationViewController];
        svc.alignmentSegments.selectedSegmentIndex = 1;
    }
}

//Logout button to LoginVC
- (IBAction)pressedLogOut:(id)sender
{
    [PFUser logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

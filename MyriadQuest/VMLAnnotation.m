//
//  VMLAnnotation.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/20/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "VMLAnnotation.h"

@implementation VMLAnnotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;
@synthesize name;

-(id)initWithPosition:(CLLocationCoordinate2D)coords
{
    if (self = [super init])
    {
        self.coordinate = coords;
    }
    return self;
}

@end

//
//  SignUpViewController.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/21/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *buttonSignUp;
@property (weak, nonatomic) IBOutlet UITextField *textUser;
@property (weak, nonatomic) IBOutlet UITextField *textPassword;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedAlignment;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIImageView *imageSignUp;
@property (weak, nonatomic) IBOutlet UITextField *textHero;

- (IBAction)pressedSignUp:(id)sender;
- (IBAction)pressedKeySlayer:(id)sender;
- (IBAction)pressedCancel:(id)sender;

@end

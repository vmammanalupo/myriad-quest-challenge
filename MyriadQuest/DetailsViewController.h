//
//  DetailsViewController.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestInfo.h"
#import <MapKit/MapKit.h>

@interface DetailsViewController : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelQName;
@property (weak, nonatomic) IBOutlet UILabel *labelQGName;
@property (weak, nonatomic) IBOutlet UILabel *labelQExp;
@property (weak, nonatomic) IBOutlet UILabel *labelQuestStatus;
@property (weak, nonatomic) IBOutlet UITextView *textDetails;
@property (weak, nonatomic) IBOutlet UIImageView *imageQGiver;
@property (weak, nonatomic) IBOutlet UIButton *buttonAccept;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic)QuestInfo *detailViewInfo;

- (IBAction)pressedAccept:(id)sender;

@end

//
//  TableViewController.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestInfo.h"
#import "CustomTableViewCell.h"

@interface TableViewController : UITableViewController

- (IBAction)pressedLogOut:(id)sender;

@end

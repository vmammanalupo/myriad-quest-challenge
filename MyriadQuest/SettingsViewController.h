//
//  SettingsViewController.h
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) NSString *alignment;
@property (weak, nonatomic) IBOutlet UITextField *textHeroName;
@property (weak, nonatomic) IBOutlet UIImageView *imageSettings;
@property (weak, nonatomic) IBOutlet UISegmentedControl *alignmentSegments;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *labelPlayerPortrait;
@property (weak, nonatomic) IBOutlet UIImageView *imagePlayer;

- (IBAction)pressedKeySlayer:(id)sender;
- (IBAction)pressedCancel:(id)sender;
- (IBAction)pressedSave:(id)sender;

@end

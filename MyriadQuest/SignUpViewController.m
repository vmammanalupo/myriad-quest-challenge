//
//  SignUpViewController.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/21/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "SignUpViewController.h"
#import <Parse/Parse.h>

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _segmentedAlignment.tintColor = [UIColor purpleColor];
    _segmentedAlignment.backgroundColor = [UIColor whiteColor];
    
    _buttonSignUp.titleLabel.textColor = [UIColor purpleColor];
    _buttonSignUp.titleLabel.backgroundColor = [UIColor whiteColor];
    
    _buttonCancel.titleLabel.textColor = [UIColor purpleColor];
    _buttonCancel.titleLabel.backgroundColor = [UIColor whiteColor];
    
    _imageSignUp.image = [UIImage imageNamed:@"settingsBack.png"];
}

//method call for return button to remove keyboard
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//saving username and password to parse/plist
- (IBAction)pressedSignUp:(id)sender
{
    if (!([_textHero.text isEqualToString:@""]) && (![_textUser.text isEqualToString: @""]) && (![_textPassword.text isEqualToString:@""]))
    {
        PFUser *user = [PFUser user];
        user.username = _textUser.text;
        user.password = _textPassword.text;
        user [@"name"] = _textHero.text;
        user [@"alignment"] = [NSNumber numberWithInt:_segmentedAlignment.selectedSegmentIndex];
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
        {
            if (!error)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject: _textUser.text  forKey:@"username"];
                [defaults setObject: _textPassword.text forKey:@"password"];
                [defaults setObject:_textHero.text forKey:@"heroName"];
                [defaults synchronize];
                
                if (_segmentedAlignment.selectedSegmentIndex == 0)
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:@"Good" forKey:@"Alignment"];
                    [defaults synchronize];
                }
                else if (_segmentedAlignment.selectedSegmentIndex == 1)
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:@"Neutral" forKey:@"Alignment"];
                    [defaults synchronize];
                }
                else if (_segmentedAlignment.selectedSegmentIndex == 2)
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:@"Evil" forKey:@"Alignment"];
                    [defaults synchronize];
                }
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                UIAlertView *loginFailed = [[UIAlertView alloc]initWithTitle:@"Login Failed"
                message:@"The username entered is taken!"
                delegate:nil
                cancelButtonTitle:@"Okay"
                otherButtonTitles: nil];
                
                [loginFailed show];
            }
        }];
    }
    else
    {
        UIAlertView *signUpFailed = [[UIAlertView alloc]initWithTitle:@"Sign Up Failed"
        message:@"All text fields are required for sign up!"
        delegate:nil
        cancelButtonTitle:@"Okay"
        otherButtonTitles: nil];
        
        [signUpFailed show];
    }
}

//invisible button to remove an active keyboard when an area on the screen is clicked
- (IBAction)pressedKeySlayer:(id)sender
{
    [_textUser resignFirstResponder];
    [_textPassword resignFirstResponder];
    [_textHero resignFirstResponder];
}

//cancel button to take user back to login
- (IBAction)pressedCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end

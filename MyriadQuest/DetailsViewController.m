//
//  DetailsViewController.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "DetailsViewController.h"
#import "VMLAnnotation.h"
#import "VMLAnnotationsView.h"
#import <Parse/Parse.h>

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    //loading quest accepted status from plist
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _detailViewInfo.questAccepted = [defaults objectForKey:[NSString stringWithFormat:@"%d",_detailViewInfo.arrayIndex]];
    
    if ([_detailViewInfo.questAccepted isEqualToString:@"Accepted"])
    {
        _buttonAccept.hidden = YES;
        _labelQuestStatus.hidden = NO;
    }
    else
    {
        _buttonAccept.hidden = NO;
        _labelQuestStatus.hidden = YES;
    }
    
     //set the labels to pull information from Quest Info object that was passed from TableViewController. Also set labels font/text size/text color
    _labelQName.text = _detailViewInfo.questName;
    _labelQName.textColor = [UIColor purpleColor];
    _labelQName.font = [UIFont fontWithName:@"Chalkduster" size:14];
    
    
    _labelQGName.text = _detailViewInfo.questGiverName;
    _labelQGName.textColor = [UIColor purpleColor];
    _labelQGName.font = [UIFont fontWithName:@"Chalkduster" size:12];
    
    _labelQExp.text = _detailViewInfo.questExp;
    _labelQExp.textColor = [UIColor purpleColor];
    _labelQExp.font = [UIFont fontWithName:@"Chalkduster" size:16];
    
    _textDetails.text = _detailViewInfo.questDescript;
    _textDetails.textColor = [UIColor purpleColor];
    _textDetails.font = [UIFont fontWithName:@"Chalkduster" size:12];
    
    _labelQuestStatus.text = @"Accepted";
    _labelQuestStatus.textColor = [UIColor purpleColor];
    _labelQuestStatus.font = [UIFont fontWithName:@"Chalkduster" size:14];
    
    _imageQGiver.image = _detailViewInfo.questGiverImage;
    
    [self mapInfo];
}

//All the map info/annotations to be loaded
-(void)mapInfo
{
    //center
    float centerX = (_detailViewInfo.questGiverLat + _detailViewInfo.questLocLat)/2;
    float centerY = (_detailViewInfo.questGiverLong + _detailViewInfo.questLocLong)/2;
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(centerX, centerY);
    
    NSMutableArray *annotations = [[NSMutableArray alloc]init];
    CLLocationCoordinate2D location;
    VMLAnnotation *annotation;
    
    //Quest Giver Location Annotation
    location.latitude = _detailViewInfo.questGiverLat;
    location.longitude = _detailViewInfo.questGiverLong;
    annotation = [[VMLAnnotation alloc]initWithPosition:location];
    [annotation setCoordinate:location];
    annotation.title = _detailViewInfo.questGiverName;
    annotation.subtitle = [NSString stringWithFormat:@"Return %@ quest here!", _detailViewInfo.questName];
    annotation.name = @"Giver";
    [annotations addObject:annotation];
    
    //Quest Location Annotation
    location.latitude = _detailViewInfo.questLocLat;
    location.longitude = _detailViewInfo.questLocLong;
    annotation= [[VMLAnnotation alloc]initWithPosition:location];
    [annotation setCoordinate:location];
    annotation.title = _detailViewInfo.questName;
    annotation.subtitle = @"Complete quest objective here!";
    annotation.name = @"Location";
    [annotations addObject:annotation];
    
    //add to map
    [self.mapView addAnnotations:annotations];
    
    //span
    CLLocation *firstQuest = [[CLLocation alloc]initWithLatitude:_detailViewInfo.questGiverLat longitude:_detailViewInfo.questGiverLong];
    CLLocation *secondQuest = [[CLLocation alloc]initWithLatitude:_detailViewInfo.questLocLat longitude:_detailViewInfo.questLocLong];
    CLLocationDistance distanceFromPoints = [firstQuest distanceFromLocation:secondQuest];
    float distanceCalculation = distanceFromPoints * .000012;
    MKCoordinateSpan span = MKCoordinateSpanMake(distanceCalculation, distanceCalculation);
    
    //assign region to map
    [self.mapView setRegion:MKCoordinateRegionMake(center, span) animated:YES];
}

//Customization of Annotation View
-(MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"pin";
    VMLAnnotationsView *view = (VMLAnnotationsView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (view == nil)
    {
        view = [[VMLAnnotationsView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    }
    return view;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    //functionality control for the button in annotation to take to Google Maps
    VMLAnnotation *annotation = (VMLAnnotation *)view.annotation;
    [self.mapView deselectAnnotation:annotation animated:YES];
    
    VMLAnnotation *viewClicked = (VMLAnnotation*)view.annotation;
    
    if ([viewClicked.name isEqualToString:@"Giver"])
    {
        CLLocationCoordinate2D destinationCoords = CLLocationCoordinate2DMake(_detailViewInfo.questGiverLat, _detailViewInfo.questGiverLong);
        MKPlacemark *finalDestination = [[MKPlacemark alloc]initWithCoordinate:destinationCoords addressDictionary:nil];
        MKMapItem *finalLocation = [[MKMapItem alloc]initWithPlacemark:finalDestination];

        NSMutableDictionary *drivingDirections = [[NSMutableDictionary alloc]init];
        [drivingDirections setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
        [finalLocation openInMapsWithLaunchOptions:drivingDirections];
    }
    else if ([viewClicked.name isEqualToString:@"Location"])
    {
        CLLocationCoordinate2D destinationCoords = CLLocationCoordinate2DMake(_detailViewInfo.questLocLat, _detailViewInfo.questLocLong);
        MKPlacemark *finalDestination = [[MKPlacemark alloc]initWithCoordinate:destinationCoords addressDictionary:nil];
        MKMapItem *finalLocation = [[MKMapItem alloc]initWithPlacemark:finalDestination];
        
        NSMutableDictionary *drivingDirections = [[NSMutableDictionary alloc]init];
        [drivingDirections setObject:MKLaunchOptionsDirectionsModeDriving forKey:MKLaunchOptionsDirectionsModeKey];
        [finalLocation openInMapsWithLaunchOptions:drivingDirections];
    }
    else
    {
        NSLog(@"Something went horribly wrong with transition to Map App");
    }
}

//functionality for the accept quest button/saving to plist
//Still need to link to parse
- (IBAction)pressedAccept:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Accepted" forKey:[NSString stringWithFormat:@"%d",_detailViewInfo.arrayIndex]];
    
    [defaults synchronize];
    _buttonAccept.hidden = YES;
    _labelQuestStatus.hidden = NO;
}

@end

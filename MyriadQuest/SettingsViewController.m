//
//  SettingsViewController.m
//  MyriadQuest
//
//  Created by Vince Mammana-Lupo on 5/19/14.
//  Copyright (c) 2014 Vince Mammana-Lupo. All rights reserved.
//

#import "SettingsViewController.h"
#import <Parse/Parse.h>

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //setting image
    _imageSettings.image = [UIImage imageNamed:@"settingsBack.png"];
    
    //setting segmented controller color scheme
    _alignmentSegments.tintColor = [UIColor purpleColor];
    _alignmentSegments.backgroundColor = [UIColor whiteColor];
    
    //setting save/cancel button color scheme
    _saveButton.tintColor = [UIColor purpleColor];
    _saveButton.backgroundColor = [UIColor whiteColor];
    _cancelButton.tintColor = [UIColor purpleColor];
    _cancelButton.backgroundColor = [UIColor whiteColor];
    
    //setting player portrait label
    _labelPlayerPortrait.text = @"Player Portrait";
    _labelPlayerPortrait.textColor = [UIColor purpleColor];
    _labelPlayerPortrait.backgroundColor = [UIColor whiteColor];
    _labelPlayerPortrait.font = [UIFont fontWithName:@"Chalkduster" size:16];
    
    //player picture
    _imagePlayer.image = [UIImage imageNamed:@"portraitExample.jpg"];
}

//method call to remove keyboard with return
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//Having plist populate the hero name/origin text fields
-(void)viewDidAppear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _textHeroName.text = [defaults objectForKey:@"heroName"];
    _alignment = [defaults objectForKey:@"Alignment"];
    
    if ([_alignment isEqualToString:@"Good"])
    {
        _alignmentSegments.selectedSegmentIndex = 0;
    }
    else if ([_alignment isEqualToString:@"Neutral"])
    {
        _alignmentSegments.selectedSegmentIndex = 1;
    }
    else if ([_alignment isEqualToString:@"Evil"])
    {
        _alignmentSegments.selectedSegmentIndex = 2;
    }
    else
    {
        _alignmentSegments.selectedSegmentIndex = 1;
    }
}

//invisible button to remove active keyboard when an area on the screen is touched
- (IBAction)pressedKeySlayer:(id)sender
{
    [_textHeroName resignFirstResponder];
}

//set cancel button to return to the table view controller
- (IBAction)pressedCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//set save button to save text entered into textfields to the plist
- (IBAction)pressedSave:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: _textHeroName.text  forKey:@"heroName"];
    [defaults synchronize];
    
    PFUser *user = [PFUser currentUser];
    [user setObject:[NSNumber numberWithInteger:_alignmentSegments.selectedSegmentIndex]forKey:@"alignment"];
    [user setObject:[defaults objectForKey:@"heroName"] forKey:@"name"];
    [user saveInBackground];
    
    //saving alignment to plist
    if (_alignmentSegments.selectedSegmentIndex == 0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"Good" forKey:@"Alignment"];
        [defaults synchronize];
    }
    else if (_alignmentSegments.selectedSegmentIndex == 1)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"Neutral" forKey:@"Alignment"];
        [defaults synchronize];
    }
    else if (_alignmentSegments.selectedSegmentIndex == 2)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"Evil" forKey:@"Alignment"];
        [defaults synchronize];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
